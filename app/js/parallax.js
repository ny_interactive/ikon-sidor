
var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";


window.global = {
  scrollY : $(window).scrollTop(),
  pageWidth: $(document).width(),
  pageHeight: $(document).height(),
  html: $('html'),
	body: $('body'),
  timer: null
}

//scroll.init();

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};





function castParallax() {

	var opThresh = 350;
	var opFactor = 750;


	window.addEventListener("scroll", function(event){


		var top = this.pageYOffset,
			parallaxDelay = 0;

		var layers = document.getElementsByClassName("parallax");
		var layer, speed, yPos;
		for (var i = 0; i < layers.length; i++) {

			layer = layers[i];
			speed = layer.getAttribute('data-speed');
			parallaxDelay = parseInt( layer.getAttribute('data-delay') );
			parallaxDelay = ( isNaN(parallaxDelay) ) ? 4 : parallaxDelay;
			var yPos = -(top * speed / 100);

			console.log(yPos, parallaxDelay);

			if(parallaxDelay){

				if(yPos < - parallaxDelay){
					console.log('nu');
					var dist = Math.round(yPos + parallaxDelay + top);
					layer.setAttribute('style', 'transform: translate3d(0px, ' + dist + 'px, 0px)');
					//layer.setAttribute('style', 'transform: translate3d(0px, ' + yPos + 'px, 0px)');
				}
				else {
					layer.setAttribute('style', 'transform: translate3d(0px, ' + top  + 'px, 0px)');
					//layer.setAttribute('style', 'margin-top:' + yPos * (-1) + 'px');
				}
			}
			else {
				//layer.setAttribute('style', 'transform: translate3d(0px, ' + yPos + 'px, 0px)');
			}
			

		}
	});


}

var updatePosition = function(dist){
	layer.setAttribute('style', 'transform: translate3d(0px, ' + dist + 'px, 0px)');
}

function dispelParallax() {
	$("#nonparallax").css('display','block');
	$("#parallax").css('display','none');
}

function castSmoothScroll() {
	$.srSmoothscroll({
		step: 80,
		speed: 300,
		ease: 'linear'
	});
}



function startSite() {

	var platform = navigator.platform.toLowerCase();
	var userAgent = navigator.userAgent.toLowerCase();

	if ( platform.indexOf('ipad') != -1  ||  platform.indexOf('iphone') != -1 ) 
	{
		dispelParallax();
		console.log('ios');
	}
	
	else if (platform.indexOf('win32') != -1 || platform.indexOf('linux') != -1)
	{
		console.log('win32');

		castParallax();					
		if ($.browser.webkit)
		{
			castSmoothScroll();
		}
	}
	
	else
	{
		console.log('default');
		//window.requestAnimationFrame(castParallax);
		castParallax();
	}

}

document.body.onload = startSite();