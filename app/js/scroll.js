
var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";


window.global = {
  scrollY : $(window).scrollTop(),
  pageWidth: $(document).width(),
  pageHeight: $(document).height(),
  html: $('html'),
	body: $('body'),
  timer: null
}

window.scroll = {
	
	init: function(){

		/*$('body').bind(mousewheelevt, function(e){

		    var delta = scroll.getDelta(e);

		    if( delta < -20 ){
		    	$('body').addClass('scrolling');
		    }
		    else {
		    	$('body').removeClass('scrolling');
		    }

		});*/

		$(window).scroll(function () {
		  var scroll = $(window).scrollTop();
		  
		  if(global.timer !== null) {
		    clearTimeout(global.timer); 
		    
		    if( scroll > global.scrollY ){
		      	$('body').addClass('scrolling');
		    }
		    else {
		     	$('body').removeClass('scrolling');
		    }
		  }
		  global.timer = setTimeout(function() {
		    // do something
		    //console.log('stopped scrolling');
		    global.scrollY = scroll;
		  }, 50);
		  
		});

	},

	getDelta: function(e){
	    var evt = window.event || e ;
	    evt = evt.originalEvent ? evt.originalEvent : evt;
	    return evt.detail ? evt.detail*(-40) : evt.wheelDelta;
	}
}

//scroll.init();

function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	};
};



//var bannerMedia = document.querySelector(".parallax"), lastScrollY = 0, ticking = false, translateValue = 0, speedDivider = 3.75, ww = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

/*if (bannerMedia === null || global.pageWidth <= 768) {
    return false;
}*/
// Update background position
/*var updatePosition = function() {
    translateValue = lastScrollY / speedDivider;
    if (translateValue < 0) translateValue = 0;
    
    translateY(bannerMedia, translateValue);

    ticking = false;
};

var translateY = function(element, value) {
    $(element).css('transform','translateY(' + Math.round(value) + 'px)');
};

var requestTick = function() {
    if (!ticking) {
        window.requestAnimationFrame(updatePosition);
        ticking = true;
    }
};

var doScroll = function() {
    lastScrollY = window.scrollY;
    requestTick();
};

window.addEventListener("scroll", debounce(doScroll, 10), false);*/



function castParallax() {

	var opThresh = 350;
	var opFactor = 750;


	window.addEventListener("scroll", function(event){

		var top = this.pageYOffset,
			parallaxDelay = 0;

		var layers = document.getElementsByClassName("parallax");
		var layer, speed, yPos;
		for (var i = 0; i < layers.length; i++) {
			layer = layers[i];
			speed = layer.getAttribute('data-speed');
			parallaxDelay = parseInt( layer.getAttribute('data-delay') );
			var yPos = -(top * speed / 100);

			//console.log(yPos, parallaxDelay);

			if(parallaxDelay){

				if(yPos < - parallaxDelay){
					console.log('nu');
					var dist = Math.round(yPos + parallaxDelay + top);
					layer.setAttribute('style', 'transform: translate3d(0px, ' + dist + 'px, 0px)');
					//layer.setAttribute('style', 'transform: translate3d(0px, ' + yPos + 'px, 0px)');
				}
				else {
					layer.setAttribute('style', 'transform: translate3d(0px, ' + top  + 'px, 0px)');
					//layer.setAttribute('style', 'margin-top:' + yPos * (-1) + 'px');
				}
			}
			else {
				//layer.setAttribute('style', 'transform: translate3d(0px, ' + yPos + 'px, 0px)');
			}
			

		}
	});


}

var updatePosition = function(dist){
	layer.setAttribute('style', 'transform: translate3d(0px, ' + dist + 'px, 0px)');
}

function dispelParallax() {
	$("#nonparallax").css('display','block');
	$("#parallax").css('display','none');
}

function castSmoothScroll() {
	$.srSmoothscroll({
		step: 80,
		speed: 300,
		ease: 'linear'
	});
}



function startSite() {

	var platform = navigator.platform.toLowerCase();
	var userAgent = navigator.userAgent.toLowerCase();

	if ( platform.indexOf('ipad') != -1  ||  platform.indexOf('iphone') != -1 ) 
	{
		dispelParallax();
	}
	
	else if (platform.indexOf('win32') != -1 || platform.indexOf('linux') != -1)
	{

		castParallax();					
		if ($.browser.webkit)
		{
			castSmoothScroll();
		}
	}
	
	else
	{
		//window.requestAnimationFrame(castParallax);
		castParallax();
	}

}

document.body.onload = startSite();