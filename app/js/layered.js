window.layered = {
  lastPos : 0,
  py : 0,
  dY : 0,
  yPos : 0,
  wh : 0,

  init : function(){
    this.setEvents();
    this.setUpScene();
  },

  setEvents : function(){
    var _this = this;
    console.log('set setEvents');

    $(document).on('scroll', function(){

      _this.moveBlocks();
      
    });

  },


  setUpScene : function(){
    var _this = this;
    _this.py = $('.layers').offset().top;
    _this.wh = $(window).height();



    //$('.layers').height(this.wh * 2.2);
    $('.layers').height(this.wh * 1.2);

    setTimeout(function() {

      $('.layered').each(function(){
        //this.py = $(this).attr('data-y');
        
        this.yPos = $(this).attr('y-pos');
        this.dY = $(this).attr('data-direction');
        this.speed = $(this).attr('data-speed');
        
        //$(this).css('top', this.py + 'px');
        $(this).css('top', this.yPos + '%');

        this.py = $(this).offset().top;
        $(this).attr('start-pos', this.py );
        //console.log('dY: ', $(this).offset() );
        //$(this).attr('data-y', $(this).offset().top - _this.py);
      });

    }, 1400);
    
  },

  moveBlocks : function(){
    var st = $(document).scrollTop(),
    py = this.py,
    wh = this.wh,
    iw = py-wh;
    if(st > (iw)){

      var move = (py-st);

      
      $('.layered').each(function(){


        this.py = $(this).attr('start-pos');
        this.yPos = $(this).attr('y-pos');
        this.dY = $(this).attr('data-direction');
        this.speed = $(this).attr('data-speed');

        move = (this.py - st - 300);



        var newpos = (move) * (this.dY) * (this.speed);

        if(this.py - st < $(window).height() ){

          $(this).css({
            'transform':'translate(0, 0) translate3d(0, ' +  newpos   + 'px, 0)'
          });

          
        }

      });
    }

  }
}


