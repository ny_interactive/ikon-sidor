
var mousewheelevt = (/Firefox/i.test(navigator.userAgent)) ? "DOMMouseScroll" : "mousewheel";


window.globals = {
	scrollY : $(window).scrollTop(),
	pageWidth: $(document).width(),
	pageHeight: $(document).height(),
	windowHeight: $(window).height(),
	root: $('html, body'),
	html: $('html'),
	body: $('body'),
	timer: null,
	site: 'heritage',
	scrollOffset: '25%'
}

$.fn.scrollEnd = function(callback, timeout) {
  $(this).scroll(function(){


    var $this = $(this);
    if ($this.data('scrollTimeout')) {
      clearTimeout($this.data('scrollTimeout'));
    }
    $this.data('scrollTimeout', setTimeout(callback,timeout));
  });
};


var lastScrollY = 0, ticking = false, translateValue = 0, speedDivider = 1, ww = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;


window.nav = {

	checkActive: function(_id){
		$('.theme-page-menu a').removeClass('active');
		$('.theme-page-menu a[go-to-id="' + _id + '"]').addClass('active');
	}
}

window.events = {

	init: function(){


		$('[go-to-id]').on('click', function(e){
			e.preventDefault();

			var $this = $(this),
					_id = "#" + $this.attr('go-to-id');

					scroll.scrollTo(_id);


		});

		$('.play-movie').on('click', function(e){
			e.preventDefault();

			var $movie_wrapper = $('.movie-wrapper');
			var $movie_wrapper = ($(this).parent().hasClass('movie-wrapper')) ? $(this).parent() : $(this).parent().parent();


			if ( $('.movie-wrapper').hasClass('playing') ){
				movieControls.endMovie( $('.movie-wrapper.playing') );
			}

			movieControls.playMovie($movie_wrapper);
				/*$iframe = $movie_wrapper.find('iframe');

			var _src = $iframe.attr('src');

			$iframe.attr('src', _src + '&autoplay=1');

			$movie_wrapper.addClass('playing');*/
		});


		$('.toggle-features').on('click', function(e){
			e.preventDefault();

			var $this = $(this),
				prevText = $this.attr('prev-text');

				prevText = (prevText == 'Göm') ? 'Visa' : 'Göm';

				$this.attr('prev-text', prevText);

			if( $( $this.attr('href') ).hasClass('show-me') ){
				$( $this.attr('href') ).addClass('hide-me');
				$( $this.attr('href') ).removeClass('show-me');
			}
			else if( $( $this.attr('href') ).hasClass('hide-me') ){
				$( $this.attr('href') ).removeClass('hide-me');
				$( $this.attr('href') ).addClass('show-me');
			}
			else {
				$( $this.attr('href') ).addClass('hide-me');
			}


		});


		$(document).keyup(function(e) {
		     if (e.keyCode == 27 && $('.movie-wrapper').hasClass('playing') ) {
		     	var $movie_wrapper = $('.movie-wrapper.playing');

		     	movieControls.endMovie($movie_wrapper);
				/*$iframe = $movie_wrapper.find('iframe');

				var _src = $iframe.attr('src');

				$iframe.attr('src', _src + '&autoplay=0');

				$movie_wrapper.removeClass('playing');*/
		    }
		});

	},

	mobileDevice : function() {
	 if( navigator.userAgent.match(/Android/i)
		 || navigator.userAgent.match(/webOS/i)
		 || navigator.userAgent.match(/iPhone/i)
		 || navigator.userAgent.match(/iPad/i)
		 || navigator.userAgent.match(/iPod/i)
		 || navigator.userAgent.match(/BlackBerry/i)
		 || navigator.userAgent.match(/Windows Phone/i)
	 ){
	    return true;
	  }
	 else {
	    return false;
	  }
	}
}


window.movieControls = {



	autoplay : function(_eventType){

		if( $('.movie-wrapper.autoplay').length > 0 ){

			//var myVideo =  iframe.getElementById('mute');
			//myVideo.mute();

			if(_eventType == 'touch'){
				$('.theme-page').on('touchstart', function(e){
					movieControls.playMovie( $('.movie-wrapper.autoplay') );
				});
			}
			else {
				movieControls.playMovie( $('.movie-wrapper.autoplay') );
			}
		}
	},


	playMovie : function(movie_wrapper){
		var _iframe = movie_wrapper.find('iframe'),
			_src = _iframe.attr('src');

			if( _src.indexOf('autoplay') > -1 ){
				var _newSrc = _src.replace('autoplay=0', 'autoplay=1');

				_src = _newSrc;
			}
			else {
				_src += '&autoplay=1';
			}

			_iframe.attr('src', _src);

		movie_wrapper.addClass('playing');
	},

	endMovie : function(movie_wrapper){

		var _iframe = movie_wrapper.find('iframe'),
			_src = _iframe.attr('src');

		var _newSrc = _src.replace('autoplay=1', 'autoplay=0');

		_iframe.attr('src', _newSrc );

		movie_wrapper.removeClass('playing');

	}
}


window.scroll = {

	sections : {},

	controlNavigation : function(){
		if ( globals.pageWidth > 768 ) {

			$(window).on('scroll', function(){
				var scroll = $(window).scrollTop();

				if(globals.timer !== null) {
				    clearTimeout(globals.timer);

				    if( scroll > globals.scrollY  ){
				      //console.log('göm');
				      $('body').addClass('scrolling');
				    }
				    else {
				      //console.log('visa');
				      $('body').removeClass('scrolling');
				    }
				}

				globals.timer = setTimeout(function() {
				    // do something
				    //console.log('stopped scrolling');
				    globals.scrollY = scroll;
				}, 50);


			});
		}
	},

/*	init: function(){

		scroll.sections = $('[scroll]');

		$('[scroll]').each( function(){
			var $this = $(this);
			scroll.sections[ parseInt($this.position().top) ] = $this;
		});


		$(window).on('scroll', function(e){


		    console.log(parseInt( $(window).scrollTop() ));
		    console.log( scroll.sections[ parseInt( $(window).scrollTop() ) ] );

		    scroll.checkScrollPos();

		});


	},

	checkScrollPos: function(){

		var speedDivider = 1,
			scrollY = $(window).scrollTop(),
			translateValue = scrollY / speedDivider;




		scroll.sections.each( function(){
			var posY = parseInt( $(this).position().top );



			translateValue = translateValue - posY;

			console.log('translateValue: ', translateValue);

			if( scrollY >= posY && scrollY <= (posY + $(this).height() ) ){
				var deltaPositive = Math.round(translateValue/2);
				var deltaNegative = Math.round(translateValue/2) * -1;

				$(this).addClass('hit');
				$(this).find('[scroll-content]').css('transform','translateY(' + deltaNegative + 'px)');
			}
			else {
				$(this).removeClass('hit');
			}
		});
	},
*/
	scrollTo : function(_id){

		setTimeout(function() {

			//$(_id).addClass('hit fade-in');
			console.log('id: ',_id);

			globals.root.animate({
	        	scrollTop: $(_id).offset().top
	    	}, 500);



			/*setTimeout(function() {
				location.hash = id;
			}, 300);*/

		}, 300);
	},

	scrollToPos : function(_pos){

		setTimeout(function() {

			globals.root.animate({
	        	scrollTop: _pos
	    	}, 300);

			/*setTimeout(function() {
				location.hash = id;
			}, 300);*/

		}, 100);
	},

	getDelta: function(e){
	    var evt = window.event || e ;
	    evt = evt.originalEvent ? evt.originalEvent : evt;
	    return evt.detail ? evt.detail*(-40) : evt.wheelDelta;
	}
}

window.scrollPoints = {

	topOffsetScrollPoints : $('[scroll-point].offset-top'),
	contentOffsetScrollPoints : $('[scroll-point]'),
	bottomOffsetScrollPoints : $('[scroll-point-bottom]'),
	inMultipleArea : false,
	active_ID:"",

	init: function(){

		// Trigger scrollPoint with offset top

		for (var i = 0; i < this.topOffsetScrollPoints.length; i++) {
			new Waypoint({
			element: this.topOffsetScrollPoints[i],
			handler: function(direction) {

				var active = this.element;



				if(direction === 'up'){

					active = $(this.element).prev();
					$(this.element).removeClass('top-hit');


				}

				//$('[scroll].hit').removeClass('hit');
				$(active).addClass('top-hit');

				if( $(active).hasClass('banner') ){

					var scrollY = $(window).scrollTop(),
					translateValue = scrollY / speedDivider;

					var posY = parseInt( $(this.element).position().top );

					translateValue = translateValue - posY;

					scrollPoints.transformContent(translateValue, active);
				}


				// if( $(active).hasClass('zoom-in') ){
				// 	scrollPoints.zoomContent( active );
				// }

				/*if( $(active).hasClass('darken') ){
					scrollPoints.changeBackground( active );
				}

				else {
					$('.multiple').removeClass('darken');
				}	*/

				if( $(active).parent().hasClass('multiple') ){
					scrollPoints.inMultipleArea = true;
				}



				/*if(globals.site == "heritage"){

					var el = $('.banner .bg:last-of-type');

					$(window).on('scroll', function(e){

						var posY = parseInt( $(el).position().top ),
							scrollY = $(window).scrollTop(),
							translateValue = scrollY / 2;

						translateValue = translateValue - posY;

						scrollPoints.transformContent(translateValue, el);
					});
				}*/


				$(window).scrollEnd(function(){
					var activePosY = $(active).offset().top;
					var active_height = $(active).height(),

					nextPosY = ($(active).next().length != 0) ? activePosY + active_height : activePosY;

					activePosY = (activePosY < 0 ) ? activePosY * -1 : activePosY;

					if( scrollPoints.inMultipleArea && globals.pageWidth > 1024){

					    if( ( nextPosY - $(window).scrollTop() ) < ( $(window).scrollTop() - activePosY ) ) {

							scroll.scrollToPos(nextPosY);

						}
						else {

							scroll.scrollToPos(activePosY);

						}
					}
				}, 500);




				//$(this.element).find('.title').css('transform','translateY(' + translateValue + 'px)');
			},
			continuous: false,
				offset: '0%'
			})
		}

		for (var i = 0; i < this.contentOffsetScrollPoints.length; i++) {
			new Waypoint({
			element: this.contentOffsetScrollPoints[i],
			handler: function(direction) {

				var active = this.element;


				if( !$(active).parent().hasClass('multiple') ){
					scrollPoints.inMultipleArea = false;
				}


				$('body').removeClass('hide-nav');

				if(direction === 'up'){

					active = $(this.element).prev();
					$(this.element).removeClass('hit');

					$('body').removeClass('hide-nav');

					console.log('upp');
				}

				scrollPoints.active_ID = ( $(active).attr('id') != undefined ) ? $(active).attr('id') : scrollPoints.active_ID;

				nav.checkActive(scrollPoints.active_ID);

				//$('[scroll].hit').removeClass('hit');

				$(active).prevAll().addClass('hit');
				$(active).addClass('hit');

				if( $(active).hasClass('banner') ){

					var scrollY = $(window).scrollTop(),
					translateValue = scrollY / speedDivider;

					var posY = parseInt( $(this.element).position().top );

					translateValue = translateValue - posY;

					scrollPoints.transformContent(translateValue, active);


					if(direction === 'up'){
						$('body').addClass('hide-nav');
					}
					else {
						$('body').removeClass('hide-nav');
					}

				}
				else {
					$('body').removeClass('hide-nav');
				}


				if( $(active).hasClass('zoom-in') && direction != 'up' ){
					console.log('zoom-in');
					scrollPoints.zoomContent( active );
				}


				// if( $('.theme-page').hasClass('black-ice') ){
				//
				// 	$(window).on('scroll', function(e){
				//
				// 		var scrollY = $(window).scrollTop(),
				// 		translateValue = scrollY / speedDivider;
				//
				// 		var posY = parseInt( $(this.element).position().top );
				//
				// 		translateValue = translateValue - posY;
				//
				// 		var deltaPositive = Math.round(translateValue/2);
				// 		var deltaNegative = Math.round(translateValue/-2);
				//
				// 		console.log('deltaPositive: ', deltaPositive);
				// 		var scale = ( deltaPositive > 0 ) ? 1.0 + deltaPositive/600 : 1.0;
				//
				// 		console.log('scale: ', scale);
				// 		$(active).find('.lake.bg img:first').css({
				// 			'margin-bottom' : deltaNegative + 'px',
				// 			'transform' : 'scale(' + scale + ')'
				// 		});
				//
				// 		var small_scale = ((1.2 - scale/20) > 1) ? (1.2 - scale/20) : 1.0;
				// 		small_scale = ( deltaPositive > 0 ) ? 1.0 + deltaPositive/1200 : 1.0;
				//
				// 		small_scale = (small_scale < 1.2) ? small_scale : 1.2;
				// 		$(active).find('.lake.bg img:last').css({
				// 			'transform' : 'scale(' + small_scale + ')'
				// 		});
				// 	});
				// }

/*				if( $(active).hasClass('darken') ){
					scrollPoints.changeBackground( active );
				}
				else {
					$('.multiple').removeClass('darken');
				}




				$(window).scrollEnd(function(){
					var activePosY = $(active).offset().top;
					var active_height = $(active).height(),

					nextPosY = ($(active).next().length != 0) ? activePosY + active_height : activePosY;


					activePosY = (activePosY < 0 ) ? activePosY * -1 : activePosY;


					if( $(active).parent().hasClass('multiple') && ($(active).next().length != 0)){

					    if( ( nextPosY - $(window).scrollTop() ) < ( $(window).scrollTop() - activePosY ) ) {

							scroll.scrollToPos(nextPosY);

						}
						else {

							scroll.scrollToPos(activePosY);

						}
					}
				}, 500);*/




				//$(this.element).find('.title').css('transform','translateY(' + translateValue + 'px)');
			},
			continuous: false,
				offset: globals.scrollOffset
			})
		}

		// Trigger scrollPoint with offset bottom

		for (var i = 0; i < this.bottomOffsetScrollPoints.length; i++) {
			new Waypoint({
			element: this.bottomOffsetScrollPoints[i],
			handler: function(direction) {

				var active = this.element;



				if( $(active).hasClass('stop-prev') ){
					$(this.element).prev().addClass('stop');

					if(direction === 'up'){


						$('.stop').removeClass('stop');
					}
				}

				if(direction === 'up'){


					//$('.stop').removeClass('stop');

					active = $(this.element).prev();

					$(this.element).removeClass('fade-in');

				}
				else {

					//$(active).addClass('stop');
					$(active).addClass('fade-in');
				}



			},
			continuous: false,
				offset: '100%'
			})
		}
	},

	transformContent: function(translateValue, el){

		$(window).on('scroll', function(e){

			var posY = parseInt( $(el).position().top ),
				scrollY = $(window).scrollTop(),
				translateValue = scrollY / 2;

			translateValue = translateValue - posY;

			var deltaNegative = Math.round(translateValue/2) * -1;
			var deltaPositive = Math.round(translateValue/2);


			if( $('.theme-page').hasClass('black-ice') ){

				$(el).find('[scroll-bg]').css({
					'transform' : 'translateY(' + deltaNegative/2 + 'px)'
				});

			}

			$(el).find('[scroll-content]').css('transform','translateY(' + deltaNegative + 'px)');

			if( $('.theme-page').hasClass('heritage') ){

				var scale = ( deltaPositive > 0 ) ? 1.0 + deltaPositive/600 : 1.0;


				$(el).find('.bg:first').css({
					'margin-bottom' : deltaNegative + 'px',
					'transform' : 'scale(' + scale + ')'
				});

				var small_scale = ((1.2 - scale/20) > 1) ? (1.2 - scale/20) : 1.0;
				small_scale = ( deltaPositive > 0 ) ? 1.0 + deltaPositive/1200 : 1.0;

				//small_scale = (small_scale < 1.2) ? small_scale : 1.2;
				small_scale = (small_scale < 1.4) ? small_scale : 1.4;

				if( deltaPositive < (globals.windowHeight/1.2) ){

					$(el).find('.bg:last').css({
						'margin-bottom' : deltaNegative/4 + 'px',
						'transform' : 'scale(' + small_scale + ')'
					});
					// $(el).find('.bg:last').css({
					// 	'transform' : 'scale(' + small_scale + ') translateY(' + deltaPositive/2 + 'px)'
					// });

					var skyline_scale = ((small_scale/1.2) < 1.2) ? (small_scale/1.2) : 1.2;
					var transY = (skyline_scale == 1.2) ? -200 : deltaPositive/-2;
					$(el).find('.bg.skyline').css({
						'margin-bottom' : (deltaNegative/8)*-1 + 'px',
						'transform' : 'scale(' + skyline_scale + ')'
					});
				}




				// $(el).find('.bg.skyline2').css({
				// 	'margin-bottom' : (deltaNegative/10)*-1 + 'px',
				// 	'transform' : 'scale(' + skyline_scale + ')'
				// });
			}
		});
	},

	changeBackground: function(el){

		$(el).parent().addClass('darken');
	},

	zoomContent: function(el){

		var startScrollPos = $(window).scrollTop();
		// var startScrollPos = $(window).scrollTop() - (globals.windowHeight * ( parseInt(globals.scrollOffset)/100) );
		var stopScrollPos = $(window).scrollTop();

		$(window).on('scroll', function(e){
			var diff = ( ( $(window).scrollTop() - startScrollPos )/200 ) / 12;
			// var diff =  ( $(window).scrollTop() - startScrollPos );
			//var diff = ( ( $(window).scrollTop() - startScrollPos )/100 ) / 20;
			//console.log(diff);
			//var scale = ( diff > 0 ) ? 0.5 + diff : 0.5;
			var scale = ( diff > 0 ) ? 1.0 + diff : 1.0;

			/*
			if( scale <= 1.3 ){

				$(el).find('[scroll-bg]').css('transform','scale(' + scale + ') translateY(0)');
				stopScrollPos = $(window).scrollTop();
			}
			else {
				$(el).find('[scroll-bg]').css('transform','scale(1.3) translateY(-' + ($(window).scrollTop() - stopScrollPos ) + 'px)');
			}*/

			$(el).find('[scroll-bg]').css('transform','scale(' + scale + ') translateY(0)');
		});
	}
}






window.flipIt = {
  elements : $('.flip-it'),

  init: function(){

		if(flipIt.elements.length > 0){
	    flipIt.calcVars();
	    flipIt.setEvents();
		}
  },

  calcVars : function(){

    // Calculate each perspective
    flipIt.elements.each(function(){
      var $this = $(this),
          $flipObj = $this.find('.flip-it__obj');

      $this.attr('perspective', $flipObj.width());

      var _perspective = $this.attr('perspective') + 'px' ;

       $this.css({
         'perspective': _perspective
       });
    });

  },

  setEvents : function(){


    flipIt.elements.on('mousemove', function(e){
      var $this = $(this),
          $flipObj = $this.find('.flip-it__obj'),
          _posLeft = $this.offset().left,
          _posTop = $this.offset().top,
          _thisW = $this.width(),
          _thisH = $this.height(),
          _posXCenter = _posLeft + (_thisW / 2),
          _posYCenter = _posTop + (_thisH / 2);



      var _posX = e.pageX - _posXCenter,
          _posY = e.pageY - _posYCenter;

      var _posDiff = $(window).width() * 0.1,
          _degDiff = $(window).width() * 0.3;

      var _degX = _posX/_degDiff,
          _degY = _posY/_degDiff;

      var _shineClass = 'top-left';

      if( (_degX < 0 && _degY < 0) || (_degX > 0 && _degY > 0)){
        _degX = _degX *-1;
        _degY = _degY ;

        if(_degX < -0.2){
          _shineClass = 'top-right';
        }
        else if(_degX > 0.2){
          _shineClass = 'top-left';
        }
        else {
          //_shineClass = 'top-left';
          _shineClass="";
        }

      }

      else {
        _degX = _degX ;
        _degY = _degY *-1;

        if(_degY < -0.2){
          _shineClass = 'top-left';
        }
        else if(_degY > 0.2){
          _shineClass = 'top-right';
        }
        else {
          _shineClass = '';
        }
      }

      $flipObj.css({
        transform: 'translate3d(' + _posX/_posDiff + 'px, ' + _posY/_posDiff + 'px,0) ' + 'rotateX(' + _degX + 'deg) rotateY(' + _degY + 'deg) scale(1.01)'
      });

      // Shine
      $this.removeClass('bottom-left top-left top-right bottom-right');
      $this.addClass(_shineClass);


    }).on('mouseleave', function(e){
      $('.flip-it__obj').css({
        transform: 'none'
      });

      $('.flip-it').removeClass('bottom-left top-left top-right bottom-right');
    });
  }
}





$('body').addClass('loading');

$(document).ready(function(){



	$('body').addClass('hide-nav');

	// Check if touch device
	//console.log(navigator.userAgent);

	if( events.mobileDevice() ){
		//console.log('mobileDevice');
		$('html').addClass('touch-device');

		movieControls.autoplay('touch');
	}
	else {
		//console.log('not a mobileDevice');
		movieControls.autoplay('desktop');
	}

	//scroll.controlNavigation();

	setTimeout(function() {
		//scroll.init();

		$('body').removeClass('loading');

		flipIt.init();


		events.init();
		sliders.init();
		$('.movement-layers').parallax();
		setTimeout(function() {
			scrollPoints.init();

			//document.getElementById("ice_video").playbackRate = .6;

		}, 1000);



		if($('.layered').length && globals.pageWidth > 768){
		    layered.init();
				globals.scrollOffset = '35%';
		}
		else {
			globals.scrollOffset = '45%';
		}

		if( window.location.hash ){
			// console.log('hash: ', window.location.hash);
			setTimeout(function () {
				scroll.scrollTo( window.location.hash);
			}, 500);


// console.log($(window.location.hash).parent().parent().parent().hasClass('slider'));
			if( $(window.location.hash).parent().parent().parent().hasClass('slider') ){
				// console.log('parent: ', $(window.location.hash).parent().parent().parent());
				$(window.location.hash).parent().parent().parent().addClass('hit fade-in');
			}
			else {
				$(window.location.hash).addClass('hit fade-in');
			}
		}
		else if( $(window).scrollTop() < $('.banner').position().top ){

			scroll.scrollTo( $('.banner') );
			setTimeout(function() {
				$('.banner').addClass('hit');
			}, 200);

		}

	}, 1000);

});

$(window).on('resize', function(){


	setTimeout(function() {

		$('.movement-layers').parallax();
		setTimeout(function() {
			scrollPoints.init();

		}, 1000);

		if($('.layered').length){
		    layered.init();
		}

	}, 300);
});
